Authors: Tian Tuo You, Dustin Chiasson, Fernando Aaron.

User manual:
1: Add “-g” as program parameter to launch as GUI app.
2: Within console app, type "^(index)" to use specific history result, type "ANS" to use last result.
3: Only “+”, “-” and “*” operator is supported.
4: Within console app, type "C (index)" to clear specific result, type "CE" to clear all.
5: Within console app, type type "OUTPUT (directory)" to write history to a file.