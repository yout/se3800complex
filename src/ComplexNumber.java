/**
 * Author: Fernando Aron
 **/
public class ComplexNumber {

	private float realPart;
	private float imaginaryPart;

	public ComplexNumber() {
		realPart = 0;
		imaginaryPart = 0;
	}

	/**
	 * Constructs a complex number given both
	 * a realPart and imaginaryPart component
	 */
	public ComplexNumber(float realPart, float imaginaryPart){
		
		this.realPart = realPart;
		this.imaginaryPart = imaginaryPart;
		
	}

	public float getReal(){

		return realPart;

	}

	public void setReal(float realPart){

		this.realPart = realPart;

	}
	
	public float getImaginary(){
		
		return imaginaryPart;
		
	}
	
	public void setImaginary(float imaginaryPart){
		
		this.imaginaryPart = imaginaryPart;
		
	}

	/**
	 * Set complex value
	 * @param realPart Real part of complex number
	 * @param imaginaryPart Imaginary part of complex number
	 */
	public void setComplex(float realPart, float imaginaryPart){
		this.realPart = realPart;
		this.imaginaryPart = imaginaryPart;
	}
	
	/**
	 * Creates the string representing
	 * the complex number
	 */
	public String toString(){

		if(realPart == 0 && imaginaryPart != 0) {
			return imaginaryPart + "i";

		} else if(realPart != 0 && imaginaryPart == 0) {
			return String.valueOf(realPart);

		} else {
			return realPart + "+" + imaginaryPart + "i";
		}
		
	}
	
	/**
	 * Adds two complex numbers
	 */
	public ComplexNumber add(ComplexNumber complexNumber){

		ComplexNumber result = new ComplexNumber(realPart + complexNumber.getReal(), imaginaryPart + complexNumber.getImaginary());
		return result;
		
	}
	
	/**
	 * Adds two complex numbers
	 */
	public ComplexNumber subtract(ComplexNumber complexNumber){

		ComplexNumber result = new ComplexNumber(realPart - complexNumber.getReal(), imaginaryPart - complexNumber.getImaginary());
		return result;
		
	}
	
	/**
	 * Multiplies two complex numbers
	 */
	public ComplexNumber multiply(ComplexNumber complexNumber){

		ComplexNumber result = new ComplexNumber(realPart * complexNumber.getReal() - imaginaryPart * complexNumber.getImaginary(), realPart * complexNumber.getImaginary() + imaginaryPart * complexNumber.getReal());
		return result;
		
	}
}
