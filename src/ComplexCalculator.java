/**
 * Author: Tian Tuo You (GUI and console interface, complex number calculate functionality, history functionality), Dustin Chiasson (output to file function, exception handling)
 *
 * Note: GUI is been disabled so test can reflect accurate coverage.
 **/

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.MouseEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.util.Scanner;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Pattern;

public class ComplexCalculator {
	private DefaultListModel<String> history;
	private JTextField firstNum;
	private JTextField operator;
	private JTextField secondNum;
	private JTextField focusedTextField;
	private JLabel resultLabel;
	private JList<ComplexNumber> historyList;

	/**
	 * Initialize as GUI app
	 */
	/*public void initGUI() {
		JFrame window = new JFrame("Complex Number Calculator");
		window.setTitle("Complex Number Calculator");
		window.setSize(400,432);

		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

		JPanel textPanel = new JPanel(new GridLayout(1,5));
		firstNum = new JTextField();
		firstNum.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				focusedTextField = firstNum;
			}

			@Override
			public void focusLost(FocusEvent e) {

			}
		});
		operator = new JTextField();
		secondNum = new JTextField();
		secondNum.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				focusedTextField = secondNum;
			}

			@Override
			public void focusLost(FocusEvent e) {

			}
		});
		resultLabel = new JLabel();
		textPanel.add(firstNum);
		textPanel.add(operator);
		textPanel.add(secondNum);
		textPanel.add(new JLabel("="));
		textPanel.add(resultLabel);
		textPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, 24));
		mainPanel.add(textPanel);

		JPanel upperBtnPanel = new JPanel(new GridLayout(1,2));
		JButton calculateBtn = new JButton("Calculate");
		calculateBtn.addActionListener(new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (operator.getText().isEmpty() || firstNum.getText().isEmpty() || secondNum.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Didn't type operator or operand!", "alert", JOptionPane.ERROR_MESSAGE);
				} else {
					try {
						ComplexNumber result = calculate(operator.getText(), firstNum.getText(), secondNum.getText());
						resultLabel.setText(result.toString());
						history.addElement("(" + firstNum.getText() + ") " + operator.getText() + " (" + secondNum.getText() + ")" + " = " + result);
					} catch (RuntimeException exception) {
						JOptionPane.showMessageDialog(null, exception.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		JButton lastResultBtn = new JButton("Last Result");
		lastResultBtn.addActionListener(new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (history.size() > 0) {
					focusedTextField.setText(history.lastElement().toString().split(Pattern.quote(" = "))[1]);
				} else {
					JOptionPane.showMessageDialog(null, "No history yet!", "alert", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		upperBtnPanel.add(calculateBtn);
		upperBtnPanel.add(lastResultBtn);
		upperBtnPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, 24));
		mainPanel.add(upperBtnPanel);

		JPanel historyPanel = new JPanel(new GridLayout(1,1));
		history = new DefaultListModel<>();
		historyList = new JList(history);
		historyList.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				JList list = (JList)evt.getSource();
				if (evt.getClickCount() == 2) {
					int index = list.locationToIndex(evt.getPoint());
					focusedTextField.setText(history.elementAt(index).toString().split(Pattern.quote(" = "))[1]);
				}
			}
		});
		historyPanel.add(historyList);
		textPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, 360));
		mainPanel.add(historyPanel);

		JPanel lowerBtnPanel = new JPanel(new GridLayout(1,2));
		JButton clearBtn = new JButton("Clear");
		clearBtn.addActionListener(new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (historyList.getSelectedIndex() != -1) {
					history.remove(historyList.getSelectedIndex());
				} else {
					JOptionPane.showMessageDialog(null, "No history selected!", "alert", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		JButton clearAllBtn = new JButton("Clear All");
		clearAllBtn.addActionListener(new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				history.clear();
			}
		});
		lowerBtnPanel.add(clearBtn);
		lowerBtnPanel.add(clearAllBtn);
		lowerBtnPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, 24));
		mainPanel.add(lowerBtnPanel);

		window.add(mainPanel);
		window.setVisible(true);
	}*/

	/**
	 * Initialize app as console app
	 * @param testable Specify if console is testable
	 * @param loop_time How many time will console loop, this parameter is needed otherwise loop will continue forever
	 *                  and program is not testable, this parameter is working only when testable is set to be true
	 */
	public void initConsole(boolean testable, int loop_time) {
		history = new DefaultListModel<>();
		Scanner scanner = new Scanner(System.in);
		int loop_count = 0;
		do {
			System.out.println("Computation history (Type \"C (index)\" to clear specific result, type \"CE\" to clear all, type \"OUTPUT (directory)\" to write history to a file)):");
			for (int i = 0; i < history.size(); i ++) {
				System.out.println((i + 1) + ": "+ history.get(i).toString());
			}
			System.out.println("");
			System.out.println("Please input command (Expression in format \"[num1] [operator] [num2]\", number in format \"[real_part]+[imaginary_part]i\", only \"+\", \"-\", \"*\" operator supported, type \"^(index)\" to " +
					"use specific history result, type \"ANS\" to use last result:");
			runCommand(scanner.nextLine());
			scanner.nextLine();
			loop_count ++;
		} while (!testable || loop_count < loop_time);
	}

	/**
	 * Parse and run console command
	 */
	private void runCommand(String command) {
		// Detect command user typed and perform related action
		if (command.contains("CE") && command.length() == 2) {
			history.clear();
		} else if (command.contains("C ") && command.substring(2, command.length()).matches("^[0-9]+$")) {
			int index = Integer.parseInt(command.split(" ")[1]) - 1;
			// Check if history item exist
			if (index >= 0 && index < history.size()) {
				history.remove(index);
			} else {
				System.out.println("History item didn't exist.");
			}
		} else if (command.contains("OUTPUT")) {
				writeHistoryToFile(command.split(" ")[1]);
		} else {
			command = insertHistoryResult(command); // Insert history result into expression as user requested
			ComplexNumber result;
			String[] temp; // Store first number and second number after parse expression
			try {
				// Calculate expression based on operator it used
				if (command.contains(" + ")) {
					temp = command.split(Pattern.quote(" + "));
					if (temp.length != 2) {
						System.out.println("Incorrect expression format!");

					} else {
						result = calculate("+", temp[0], temp[1]);
						System.out.println(result);
						history.addElement("(" + temp[0] + ") + (" + temp[1] + ")" + " = " + result);
					}
				} else if (command.contains(" - ")) {
					temp = command.split(Pattern.quote(" - "));
					if (temp.length != 2) {
						System.out.println("Incorrect expression format!");

					} else {
						result = calculate("-", temp[0], temp[1]);
						System.out.println(result);
						history.addElement("(" + temp[0] + ") - (" + temp[1] + ")" + " = " + result);
					}
				} else if (command.contains(" * ")) {
					temp = command.split(Pattern.quote(" * "));
					if (temp.length != 2) {
						System.out.println("Incorrect expression format!");

					} else {
						result = calculate("*", temp[0], temp[1]);
						System.out.println(result);
						history.addElement("(" + temp[0] + ") * (" + temp[1] + ")" + " = " + result);
					}
				} else {
					System.out.println("Unknown command!");
				}
			} catch (RuntimeException e) {
				System.out.println(e.getMessage());
			}
		}
	}

	/**
	 * Insert history results
	 * @param expression Expression
	 * @return Expression with history result inserted
	 */
	private String insertHistoryResult(String expression) {
		if (expression.contains("ANS")) {
			// Replace "ANS" with the last history result
			if (history.size() > 0) {
				expression = expression.replace("ANS", history.lastElement().toString().split(Pattern.quote(" = "))[1]);
			} else {
				System.out.println("No history yet!");
			}
		}

		if (expression.contains("^")) {
			// Replace "^(index)" with particular history result
			for (int i = 0; i < expression.length(); i ++) {
				if (expression.charAt(i) == '^') {
					String index = "";
					// Get index that after "^"
					for (int j = i + 1; j < expression.length(); j ++) {
						if (expression.charAt(j) < 48 || expression.charAt(j) > 57) {
							break;
						}
						index += expression.charAt(j);
					}
					// Check if successfully parsed index
					if (!index.isEmpty()) {
						if ((Integer.parseInt(index) - 1) >= 0 && (Integer.parseInt(index) - 1) < history.size()) {
							expression = expression.replace("^" + index, history.elementAt(Integer.parseInt(index) - 1).toString().split(Pattern.quote(" = "))[1]);
						} else {
							System.out.println("History item didn't exist.");
						}
					}
				}
			}
		}
		return expression;
	}

	/**
	 * Calculate two complex number.
	 * @return Computation result as ComplexNumber object
	 */
	private ComplexNumber calculate(String operator, String firstNum, String secondNum) {
		ComplexNumber num1 = strToComplexNum(firstNum);
		ComplexNumber num2 = strToComplexNum(secondNum);
		// Calculate based on operator
		if (operator.equals("+")) {
			return num1.add(num2);
		} else if (operator.equals("-")) {
			return num1.subtract(num2);
		} else if (operator.equals("*")) {
			return num1.multiply(num2);
		}
		throw new RuntimeException("Unsupported operator!");
	}

	/**
	 * Convert string to ComplexNumber object
	 * @param str String
	 * @return Converted ComplexNumber object
	 */
	private ComplexNumber strToComplexNum(String str) throws RuntimeException {
		ComplexNumber result;
		try {
			if (str.contains("+")) {
				// Parse string that contain both real part and imaginary part
				String[] temp = str.split(Pattern.quote("+"));
				if(temp.length == 2) {
					result = new ComplexNumber(Float.parseFloat(temp[0]), Float.parseFloat(temp[1].substring(0, temp[1].length() - 1)));
					return result;
				} else {
					throw new NumberFormatException("Incorrect number format!");
				}
			} else {
					// Parse string that contain only real part or imaginary part
					if (str.contains("i"))
					{
						result = new ComplexNumber(0, Float.parseFloat(str.substring(0, str.length() - 1)));
						return result;
					}
					result = new ComplexNumber(Float.parseFloat(str), 0);
				return result;
			}
		} catch(NumberFormatException e) {
			throw new NumberFormatException("Incorrect number format!");
		}
	}

	/**
	 * Write complete calculation history to file
	 * @param dir File directory
	 */
	private void writeHistoryToFile(String dir) {
		BufferedWriter bufferedWriter = null;
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(dir);
			bufferedWriter = new BufferedWriter(fileWriter);
			// Write all history into file
			for (int i = 0; i < history.size(); i ++) {
				bufferedWriter.write(i + ": " + history.get(i).toString() + "\n");
			}
		} catch (IOException e) {
			System.out.println("File directory didn't exist.");
		} finally {
			try {
				if (bufferedWriter != null) {
					bufferedWriter.close();
				}

				if (fileWriter != null) {
					fileWriter.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	public static void main(String[] args){
		ComplexCalculator complexCalculator = new ComplexCalculator();
		boolean testable = false;
		int loop_time = 0;

		// Determine weather to initialize as GUI app or console app depending on program argument
		if (args.length > 0) {
			if (args[0].equals("-g")) {
				//complexCalculator.initGUI();
				return;
			}
			if (args[0].equals("-t")) {
				testable = true;
				loop_time = Integer.parseInt(args[1]);
			}
		}
		complexCalculator.initConsole(testable, loop_time);
	}
}