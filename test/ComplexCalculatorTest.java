import org.junit.jupiter.api.*;

import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.PrintStream;
import java.io.File;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Authors: TianTuoYou (Majority of the test), Dustin Chiasson (InsertHistoryResult test)
 *
 * Explanation: This test is used to test the console application, by pre-create and record user input into ByteArrayInputStream, and
 * capture output and put them into ByteArrayOutputStream, I can test if console application is working as intended.
 */
class ComplexCalculatorTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream(); // Used to capture console output
    private final String[] programParam = new String[2];  // Program parameter contains "-t" to indicate running is for testing, and loop number.

    /**
     *  Setup ByteArrayOutputStream so all output printed by console will be stored in it
     */
    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    public void cleanUpStreams() {
        System.setOut(null);
        System.setIn(System.in);
    }

    @Test
    void calculate() {
        // Load console input
        System.setIn(new ByteArrayInputStream("3+3i + 3+3i\r\n\r\n".getBytes()));
        programParam[0] = "-t";
        programParam[1] = "1"; // 1 because there are 1 commands been typed
        ComplexCalculator.main(programParam);
        // Compare output stored in ByteArrayOutputStream with expected output
        assertTrue(outContent.toString().contains("6.0+6.0i"));


        System.setIn(new ByteArrayInputStream("5+5i - 3+3i\r\n\r\n".getBytes()));
        ComplexCalculator.main(programParam);
        assertTrue(outContent.toString().contains("2.0+2.0i"));


        System.setIn(new ByteArrayInputStream("3+3i * 5+4i\r\n\r\n".getBytes()));
        ComplexCalculator.main(programParam);
        assertTrue(outContent.toString().contains("3.0+27.0i"));


        System.setIn(new ByteArrayInputStream("3+3i NOT_AN_OPERATOR 3+3i\r\n\r\n".getBytes()));
        ComplexCalculator.main(programParam);
        assertTrue(outContent.toString().contains("Unknown command!"));


        System.setIn(new ByteArrayInputStream("3+3i / 3+3i\r\n\r\n".getBytes()));
        ComplexCalculator.main(programParam);
        assertTrue(outContent.toString().contains("Unknown command!"));


        System.setIn(new ByteArrayInputStream("3+#i + 3+#i\r\n\r\n".getBytes()));
        ComplexCalculator.main(programParam);
        assertTrue(outContent.toString().contains("Incorrect number format!"));


        System.setIn(new ByteArrayInputStream("#i + #i\r\n\r\n".getBytes()));
        ComplexCalculator.main(programParam);
        assertTrue(outContent.toString().contains("Incorrect number format!"));
    }

    @Test
    void cleanAllHistory() {
        // Load console input
        System.setIn(new ByteArrayInputStream("3+3i + 3+3i\r\n\r\nCE\r\n\r\n3+3i + 3+3i\r\n\r\n".getBytes()));
        programParam[0] = "-t";
        programParam[1] = "3";  // 3 because there are 3 commands been typed
        ComplexCalculator.main(programParam);
        assertTrue(outContent.toString().contains("6.0+6.0i"));
    }

    @Test
    void cleanHistory() {
        // Load console input
        System.setIn(new ByteArrayInputStream("1+2i + 3+4i\r\n\r\n4+5i + 6+7i\r\n\r\n8+9i + 10+11i\r\n\r\n12+13i + 14+15i\r\n\r\nC 1\r\n\r\n3+3i + 3+3i\r\n\r\n".getBytes()));
        programParam[0] = "-t";
        programParam[1] = "6";  // 6 because there are 6 commands been typed
        ComplexCalculator.main(programParam);
        assertTrue(outContent.toString().contains("1: (4+5i) + (6+7i) = 10.0+12.0i\n2: (8+9i) + (10+11i) = 18.0+20.0i\n3: (12+13i) + (14+15i) = 26.0+28.0i"));


        System.setIn(new ByteArrayInputStream("1+2i + 3+4i\r\n\r\n4+5i + 6+7i\r\n\r\n8+9i + 10+11i\r\n\r\n12+13i + 14+15i\r\n\r\nC 100\r\n\r\n3+3i + 3+3i\r\n\r\n".getBytes()));
        ComplexCalculator.main(programParam);
        assertTrue(outContent.toString().contains("History item didn't exist."));
    }

    @Test
    void outputHistory() {
        // Load console input
        System.setIn(new ByteArrayInputStream("1+2i + 3+4i\r\n\r\n4+5i + 6+7i\r\n\r\n8+9i + 10+11i\r\n\r\n12+13i + 14+15i\r\n\r\nOUTPUT /Users/TianTuoYou/Desktop/result.txt\r\n\r\n3+3i + 3+3i\r\n\r\n".getBytes()));
        programParam[0] = "-t";
        programParam[1] = "6";
        ComplexCalculator.main(programParam);
        assertTrue(new File("/Users/TianTuoYou/Desktop/result.txt").exists());

        System.setIn(new ByteArrayInputStream("1+2i + 3+4i\r\n\r\n4+5i + 6+7i\r\n\r\n8+9i + 10+11i\r\n\r\n12+13i + 14+15i\r\n\r\nOUTPUT /asd/asd/oqwie.txt\r\n\r\n3+3i + 3+3i\r\n\r\n".getBytes()));
        programParam[0] = "-t";
        programParam[1] = "6";
        ComplexCalculator.main(programParam);
        assertTrue(outContent.toString().contains("File directory didn't exist."));
    }

    @Test
    void insertHistoryResult() {
        // Load console input
        System.setIn(new ByteArrayInputStream("1+2i + 3+4i\r\n\r\n4+5i + 6+7i\r\n\r\n8+9i + 10+11i\r\n\r\n12+13i + 14+15i\r\n\r\nANS + ANS\r\n\r\n3+3i + 3+3i\r\n\r\n".getBytes()));
        programParam[0] = "-t";
        programParam[1] = "6";
        ComplexCalculator.main(programParam);
        assertTrue(outContent.toString().contains("1: (1+2i) + (3+4i) = 4.0+6.0i\n2: (4+5i) + (6+7i) = 10.0+12.0i\n3: (8+9i) + (10+11i) = 18.0+20.0i\n4: (12+13i) + (14+15i) = 26.0+28.0i\n5: (26.0+28.0i) + (26.0+28.0i) = 52.0+56.0i"));

        System.setIn(new ByteArrayInputStream("ANS + ANS\r\n\r\n".getBytes()));
        programParam[0] = "-t";
        programParam[1] = "1";
        ComplexCalculator.main(programParam);
        assertTrue(outContent.toString().contains("No history yet!"));

        System.setIn(new ByteArrayInputStream("1+2i + 3+4i\r\n\r\n4+5i + 6+7i\r\n\r\n8+9i + 10+11i\r\n\r\n12+13i + 14+15i\r\n\r\n^1 + ^3\r\n\r\n3+3i + 3+3i\r\n\r\n".getBytes()));
        programParam[0] = "-t";
        programParam[1] = "6";
        ComplexCalculator.main(programParam);
        assertTrue(outContent.toString().contains("1: (1+2i) + (3+4i) = 4.0+6.0i\n2: (4+5i) + (6+7i) = 10.0+12.0i\n3: (8+9i) + (10+11i) = 18.0+20.0i\n4: (12+13i) + (14+15i) = 26.0+28.0i\n5: (4.0+6.0i) + (18.0+20.0i) = 22.0+26.0i"));

        System.setIn(new ByteArrayInputStream("1+2i + 3+4i\r\n\r\n4+5i + 6+7i\r\n\r\n8+9i + 10+11i\r\n\r\n12+13i + 14+15i\r\n\r\n^8 + ^1\r\n\r\n3+3i + 3+3i\r\n\r\n".getBytes()));
        ComplexCalculator.main(programParam);
        assertTrue(outContent.toString().contains("History item didn't exist."));
    }

    @Test
    void historyList() {
        // Load console input
        System.setIn(new ByteArrayInputStream("3+3i + 3+3i\r\n\r\n3+3i + 3+3i\r\n\r\n3+3i + 3+3i\r\n\r\n".getBytes()));
        programParam[0] = "-t";
        programParam[1] = "3";
        ComplexCalculator.main(programParam);
        assertTrue(outContent.toString().contains("1: (3+3i) + (3+3i) = 6.0+6.0i\n2: (3+3i) + (3+3i) = 6.0+6.0i"));
    }
}