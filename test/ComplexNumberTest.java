import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by TianTuoYou on 1/8/17.
 */
class ComplexNumberTest {

    @Test
    void getReal() {
        ComplexNumber complexNumber = new ComplexNumber(10,10);
        assertEquals(10, complexNumber.getReal());
    }

    @Test
    void setReal() {
        ComplexNumber complexNumber = new ComplexNumber();
        complexNumber.setReal(10);
        assertEquals(10, complexNumber.getReal());
    }

    @Test
    void getImaginary() {
        ComplexNumber complexNumber = new ComplexNumber(10,10);
        assertEquals(10, complexNumber.getImaginary());
    }

    @Test
    void setImaginary() {
        ComplexNumber complexNumber = new ComplexNumber();
        complexNumber.setImaginary(10);
        assertEquals(10, complexNumber.getImaginary());
    }

    @Test
    void setComplex() {
        ComplexNumber complexNumber = new ComplexNumber();
        complexNumber.setComplex(10,10);
        assertEquals(10, complexNumber.getReal());
        assertEquals(10, complexNumber.getImaginary());
    }

    @Test
    void test_toString() {
        ComplexNumber complexNumber = new ComplexNumber(10,10);
        assertTrue(complexNumber.toString().equals("10.0+10.0i"));
        complexNumber.setComplex(10,0);
        assertTrue(complexNumber.toString().equals("10.0"));
        complexNumber.setComplex(0,10);
        assertTrue(complexNumber.toString().equals("10.0i"));
        complexNumber.setComplex(0,0);
        assertTrue(complexNumber.toString().equals("0.0+0.0i"));
    }

    @Test
    void add() {
        ComplexNumber complexNumber1 = new ComplexNumber(1,3);
        ComplexNumber complexNumber2 = new ComplexNumber(2,4);
        assertEquals(3, complexNumber1.add(complexNumber2).getReal());
        assertEquals(7, complexNumber1.add(complexNumber2).getImaginary());
        complexNumber1.setComplex(-4,-3);
        complexNumber2.setComplex(2,1);
        assertEquals(-2, complexNumber1.add(complexNumber2).getReal());
        assertEquals(-2, complexNumber1.add(complexNumber2).getImaginary());
        complexNumber1.setComplex(-4,-3);
        complexNumber2.setComplex(-2,1);
        assertEquals(-6, complexNumber1.add(complexNumber2).getReal());
        assertEquals(-2, complexNumber1.add(complexNumber2).getImaginary());
        complexNumber1.setComplex(1,-3);
        complexNumber2.setComplex(2,-4);
        assertEquals(3, complexNumber1.add(complexNumber2).getReal());
        assertEquals(-7, complexNumber1.add(complexNumber2).getImaginary());

    }

    @Test
    void subtract() {
        ComplexNumber complexNumber1 = new ComplexNumber(5,3);
        ComplexNumber complexNumber2 = new ComplexNumber(2,1);
        assertEquals(3, complexNumber1.subtract(complexNumber2).getReal());
        assertEquals(2, complexNumber1.subtract(complexNumber2).getImaginary());
        complexNumber1.setComplex(-5,-3);
        complexNumber2.setComplex(2,1);
        assertEquals(-7, complexNumber1.subtract(complexNumber2).getReal());
        assertEquals(-4, complexNumber1.subtract(complexNumber2).getImaginary());
        complexNumber1.setComplex(1,3);
        complexNumber2.setComplex(-2,-1);
        assertEquals(3, complexNumber1.subtract(complexNumber2).getReal());
        assertEquals(4, complexNumber1.subtract(complexNumber2).getImaginary());
        complexNumber1.setComplex(-5,-3);
        complexNumber2.setComplex(-2,-1);
        assertEquals(-3, complexNumber1.subtract(complexNumber2).getReal());
        assertEquals(-2, complexNumber1.subtract(complexNumber2).getImaginary());
    }

    @Test
    void multiply() {

        ComplexNumber complexNumber1 = new ComplexNumber(2,3);
        ComplexNumber complexNumber2 = new ComplexNumber(2,2);
        assertEquals(-2, complexNumber1.multiply(complexNumber2).getReal());
        assertEquals(10, complexNumber1.multiply(complexNumber2).getImaginary());
        complexNumber1.setComplex(2,3);
        complexNumber2.setComplex(-2,-2);
        assertEquals(2, complexNumber1.multiply(complexNumber2).getReal());
        assertEquals(-10, complexNumber1.multiply(complexNumber2).getImaginary());
        complexNumber1.setComplex(-2,3);
        complexNumber2.setComplex(2,-2);
        assertEquals(2, complexNumber1.multiply(complexNumber2).getReal());
        assertEquals(10, complexNumber1.multiply(complexNumber2).getImaginary());
        complexNumber1.setComplex(-2,-3);
        complexNumber2.setComplex(-2,-2);
        assertEquals(-2, complexNumber1.multiply(complexNumber2).getReal());
        assertEquals(10, complexNumber1.multiply(complexNumber2).getImaginary());
    }

}